﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise01
{
    class Program
    {
        public delegate bool Matcher(int i);

        static void Main(string[] args)
        {
            List<int> numbersLessThan50 = FindAll(IntGeneratorService.GenerateListOfRandomInts(20), LessThan50);
            PrintListToConsole("Alle tal mindre end 50: ", numbersLessThan50);
            List<int> evenNumbers = FindAll(IntGeneratorService.GenerateListOfRandomInts(20), IsEven);
            PrintListToConsole("Alle lige tal: ", evenNumbers);
            List<int> squareNumbers = FindAll(IntGeneratorService.GenerateListOfRandomInts(50), IsSquareNumber);
            PrintListToConsole("Alle kvadrat tal: ", squareNumbers);
            Console.WriteLine("Done...");
            Console.Read();
        }

        private static List<int> FindAll(List<int> ListToSearch, Matcher matcher)
        {
            List<int> matchingElements = new List<int>();
            foreach (var candidate in ListToSearch)
            {
                if (matcher(candidate))
                {
                    matchingElements.Add(candidate);
                }
            }
            return matchingElements;
        }

        private static bool LessThan50(int numberToTest)
        {
            return numberToTest < 50;
        }

        private static bool IsEven(int numberToTest)
        {
            return (numberToTest % 2) == 0;
        }

        private static bool IsSquareNumber(int numberToTest)
        {
            double sqrt = Math.Sqrt(numberToTest);
            return (Math.Floor(sqrt) == sqrt);
        }


        private static void PrintListToConsole(string msg, List<int> listToPrint)
        {
            Console.Write(msg);
            Console.Write("[ ");
            foreach (int i in listToPrint)
            {
                Console.Write(i + " ,");
            }
            Console.WriteLine(" ]");
        }
    }
}
