﻿using Exercise02;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise06
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> persons = Service.GetListOfPersons();

            var personsOrderedByName = persons.OrderBy(p => p.Name);
            personsOrderedByName.ToList<Person>().ForEach(p => Console.WriteLine(p));

            var personsOrderedNyNameSQLStyle = from p in persons
                                               orderby p.Name
                                               select p;
            personsOrderedNyNameSQLStyle.ToList<Person>().ForEach(p => Console.WriteLine(p));

            Console.WriteLine("Done...");
            Console.Read();
        }
    }
}
