﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise05
{
    public class Book
    {
        public string Author { get; set; }
        public string Name { get; set; }
        public DateTime Published { get; set; }
    }
}
