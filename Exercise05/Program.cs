﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise05
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Book> books = Service.GetListOfBooks();

            //Uncomment lines below, and enter the correct lambda expression in the FindAll calls
            //List<Book> booksPublishedAfter1995 = books.FindAll();
            //List<Book> booksPublishedBetween1991And1997 = books.FindAll(); 
        }
    }
}
