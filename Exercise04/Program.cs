﻿using Exercise02;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise04
{
    class Program
    {
        static void Main(string[] args)
        {
            PersonsClass persons = new PersonsClass();

            persons.Persons.ForEach(person => Console.WriteLine(person));
            Console.WriteLine();

            // Implement the SetAccepted method in the PersonsClass class
            // persons.SetAccepted(p => p.Score >= 6 && p.Age <= 40);

            persons.Persons.ForEach(person => Console.WriteLine(person));

            Console.WriteLine("Done...");
            Console.Read();

        }
    }

}
